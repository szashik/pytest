import random
import string

capitals = {'Alabama': 'Montgomery', 'Alaska': 'Juneau', 'Arizona': 'Phoenix', 'Arkansas': 'Little Rock', 'California': 'Sacramento', 'Colorado': 'Denver', 'Connecticut': 'Hartford', 'Delaware': 'Dover', 'Florida': 'Tallahassee','Georgia': 'Atlanta', 'Hawaii': 'Honolulu', 'Idaho': 'Boise', 'Illinois': 'Springfield', 'Indiana': 'Indianapolis', 'Iowa': 'Des Moines', 'Kansas': 'Topeka', 'Kentucky': 'Frankfort', 'Louisiana': 'Baton Rouge', 'Maine': 'Augusta', 'Maryland': 'Annapolis', 'Massachusetts': 'Boston', 'Michigan': 'Lansing', 'Minnesota': 'Saint Paul', 'Mississippi': 'Jackson', 'Missouri': 'Jefferson City', 'Montana': 'Helena', 'Nebraska': 'Lincoln', 'Nevada': 'Carson City', 'New Hampshire': 'Concord', 'New Jersey': 'Trenton', 'New Mexico': 'Santa Fe', 'New York': 'Albany', 'North Carolina': 'Raleigh', 'North Dakota': 'Bismarck', 'Ohio': 'Columbus', 'Oklahoma': 'Oklahoma City', 'Oregon': 'Salem', 'Pennsylvania': 'Harrisburg', 'Rhode Island': 'Providence', 'South Carolina': 'Columbia', 'South Dakota': 'Pierre', 'Tennessee': 'Nashville', 'Texas': 'Austin', 'Utah': 'Salt Lake City', 'Vermont': 'Montpelier', 'Virginia': 'Richmond', 'Washington': 'Olympia', 'West Virginia': 'Charleston', 'Wisconsin': 'Madison', 'Wyoming': 'Cheyenne'}
copyCapitals = capitals.copy() #make a copy for creating random options and non-repeating question
queCity = 'N/A' #var for question city
queCap = 'N/A' #var for question capital
tempLatter = list(map(chr, range(97,101))) # this is redicols thing for that F**king a,b,c,d

def getQuestion(): #this is for genaration questions
  ques = random.sample(copyCapitals,1) # get the city name for question
  del copyCapitals[ques[0]] #deleting it from copy of capitals to not making returdent result
  return ques

def getOptions(): #this is for genaration options
  options = random.sample(capitals.values(),3)
  options.append(queCap)
  optionRand = random.sample(options,4)
  print optionRand
  return optionRand

if __name__ == '__main__':
  quesNo = 1
  quizScore = 0

  while(len(copyCapitals)>0):
    question = getQuestion()
    queCity = question[0]
    queCap = capitals[queCity]
    #print 'cap is: ' + queCap

    print '\n%d .What is the Capital of %s?' %(quesNo, queCity)
    opt = getOptions()
    for i in xrange(0,4):
      print tempLatter[i] + '. ' + opt[i]
    ans = raw_input("Answer: ")
    if opt[ord(ans)-97]==queCap:
      print 'Correct Answer!!!!'
      quizScore+=1
    else:
      print 'Wrong Answer!!!! Correct Answer is ' + queCap

    quesNo+=1
  print 'Score: '+ str(quizScore)
